# Builds xterm.js

Provides releases of xterm.js. Releases can be found in artifacts.

[https://gitlab.com/pgregoire-ci/xtermjs/-/jobs/artifacts/master/raw/xtermjs.tar.gz?job=build](https://gitlab.com/pgregoire-ci/xtermjs/-/jobs/artifacts/master/raw/xtermjs.tar.gz?job=build)
